const fs = require('fs');


function Impresion(){

    //Declaración de variables
    const MiArray = [];
    let init = 100;
    let fin = 700000;
    let message = '';
    let numero;
    let impresion = '';

    //Validacion de existencia de carpeta
    if (!fs.existsSync('salida-collatz')) {
        fs.mkdirSync('salida-collatz');
    }

    function aleatorio(init, fin) {
        return Math.floor(Math.random() * (fin)) + init;
    }

    for (let i = 1; i <= 500; i++) {
        cont = aleatorio(init, fin);
        MiArray.push(cont);
    }

    MiArray.forEach(function (e) {
        numero = e;
        message = '';
        while (numero>1) {
            if (numero%2===0) {
                message += '\t'+(numero/2)+'\t\n';
                numero /= 2;
            } else {
                message += '\t'+((3*numero)+1)+'\t\n';
                numero = (numero*3)+1;
            }
        }
        impresion += message;
    });

    fs.writeFileSync('salida-collatz/collatz.txt', impresion, (err)=> {
        if (err) throw err;
        console.log(`Conjetura de Collatz => Archivo Creado`);
    });
    
}

exports.Impresion = Impresion;